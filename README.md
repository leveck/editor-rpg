# Editor RPG

A simple way to solo RPG in vim.

## Installation

Feel free to use the _female_names_,  _male_names_, or _names_ (that has masculine and feminine names) to assign the **namfil** variable in the **play** script. This is what the character generator uses to randomly select a name. After you sort that out:

```
make install
```

Note: sudo not needed. The makefile specifies ~/.local/bin (make sure this is in your $PATH) as the spot for the executables, and ~/.local/share/eRPG as the location for the data files.

## Playing

The rules file is opened in a tab and the hex file with a randomly generated character is opened in a tab. You use your editor to annotate the hex map in accordance with the rules. I suggest modifying .vimrc to include a key mapping for executing rdice (```:!rdice 3d6<CR>```) to roll dice from within vim.

This was made with Tunnels & Trolls in mind. The file ~/.local/share/eRPG/TnT-free.pdf is the official free rule book to help you out. You are of course free to modify this to use any rules of your choosing. The files in ~/.local/share/eRPG would just need editing.

I usually play as the character and use the _gme_ as the Game Master. There are a number of different ways to RPG solo, and you are free to experiment and find what works best for you. A quick internet search for **solo RPG** will locate a wealth of resources and suggestions.

# Also Included

_gme_ is the **Game Master Emulator**, it features a oracle, and other useful tools. (Mapping a binding to ```:!gme<CR>``` would also be a good idea.) _namegen_ generates names at random and is also ran from _gme_ but can be ran separately.

## Screenshot

![Editor RPG in action](eRPG.png)
