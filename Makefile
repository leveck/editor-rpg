#
# Makefile
# leveck, 2020-01-20 19:00
#

install:
	mkdir -p ~/.local/bin
	mkdir -p ~/.local/share/eRPG
	cp female_names ~/.local/share/eRPG
	cp hex ~/.local/share/eRPG
	cp male_names ~/.local/share/eRPG
	cp rules ~/.local/share/eRPG
	cp names ~/.local/share/eRPG
	cp adjectives ~/.local/share/eRPG
	cp nouns ~/.local/share/eRPG
	cp verbs ~/.local/share/eRPG
	cp TnT-free.pdf ~/.local/share/eRPG
	cp play ~/.local/bin
	chmod 0755 ~/.local/bin/play
	cp rdice ~/.local/bin
	chmod 0755 ~/.local/bin/rdice
	cp namegen ~/.local/bin
	chmod 0755 ~/.local/bin/namegen
	cp gme ~/.local/bin
	chmod 0755 ~/.local/bin/gme
	cp answer ~/.local/bin
	chmod 0755 ~/.local/bin/answer
	@echo
	@echo "Installed."
	@echo
	@echo "Ensure that ~/.local/bin is in your path."
	@echo "Edit ~/.local/bin/play to modify the game."
	@echo "Run 'play' to play."
# vim:ft=make
#
